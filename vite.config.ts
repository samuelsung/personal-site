import { defineConfig } from 'vite'; // eslint-disable-line import/no-extraneous-dependencies
import solidPlugin from 'vite-plugin-solid'; // eslint-disable-line import/no-extraneous-dependencies
import eslintPlugin from 'vite-plugin-eslint'; // eslint-disable-line import/no-extraneous-dependencies
import tsconfigPlugin from 'vite-tsconfig-paths'; // eslint-disable-line import/no-extraneous-dependencies

export default defineConfig({
  plugins: [solidPlugin(), tsconfigPlugin(), eslintPlugin()],
  server: {
    port: 3000,
  },
  build: {
    target: 'esnext',
  },
});
