import type { Component } from 'solid-js';

import logo from './logo.svg';

const App: Component = () => (
  <div class="text-center">
    <header
      class="
        flex flex-col items-center justify-center
        text-white xl:text-3xl md:text-2xl sm:text-xl
        bg-gray-800
        h-screen
      "
    >
      <img
        src={logo}
        class="
          pointer-events-none
          h-[40vmin] animate-[spin_20s_linear_infinite]
        "
        alt="logo"
      />
      <p>
        Edit <code>src/App.tsx</code> and save to reload.
      </p>
      <a
        class="text-purple-600"
        href="https://github.com/solidjs/solid"
        target="_blank"
        rel="noopener noreferrer"
      >
        Learn Solid
      </a>
    </header>
  </div>
);

export default App;
